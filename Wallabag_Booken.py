# -*-coding: utf-8 -*

import time
import requests
import os
import sys
import lxml.html
import lxml
import re
from bs4 import BeautifulSoup
import subprocess
from datetime import date
#définit la base de l'url du site
url = 'https://framabag.org'
user = "user"

#définit les informations de login
payload = {'_username':'username', '_password':'password', 'send' : '', '_remember_me' : 'on', '_csrf_token': ''}

#fonction principale
def main():
    list = []
    ajout = True
    #Ouvre une session (qui conserve les cookies)
    with requests.Session() as s:
        #find hidden inputs in html page
        login_html = lxml.html.fromstring(s.get(url+'/login').text)
        hidden_inputs = login_html.xpath(r'//form//input[@type="hidden"]')
        #Complete the payload dictionnary with the value of the token
        form = {x.attrib["name"]: x.attrib["value"] for x in hidden_inputs}
        payload['_csrf_token'] = form['_csrf_token']
        #make the login post request
        r1 = s.post(url+'/login_check', data = payload)
        #make the request to get the epub format
        r = s.get(url+"/export/unread.epub?tag=", stream = True)
        #save the response in a file
        title = "/home/"+ user + "/Articles_Wallabag/Articles_" + str(date.today())+".epub"
        if r.status_code == 200:
            with open(title, 'wb') as fd:
                for chunk in r.iter_content(chunk_size=128):
                    fd.write(chunk)
       
        if os.path.exists("/media/"+ user + "/Cybook") and os.path.exists(title):
            os.system("cp " + title + " /media/"+ user + "/Cybook/Articles/ && umount /media/"+ user + "/Cybook") 
            print("Archivage des articles...")
            r = ''
            for i in range (10, 0, -1):
                print(i)
                try:
               
                    r = s.get(url + "/unread/list/" +str(i)).text
                    soup = BeautifulSoup(r, "lxml")
                    for elt in soup.find_all(href = re.compile(r"/archive/[0-9]{7}")):
                        print(elt["href"])
                        s.get(url + elt["href"])
                except:
                    break
            print("Archivage terminé")
        else:
            print("Merci de brancher la liseuse ou de vérifier la présence de l'ePub")


#lance la fonction main
if __name__ == "__main__":
    main()        
        
        
        
    